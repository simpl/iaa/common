package eu.europa.ec.simpl.common.test;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

public class TestDataUtil {

    private TestDataUtil() {
        throw new UnsupportedOperationException("Utility class");
    }

    public static <T> Page<T> aPageOf(Class<T> type) {
        return new PageImpl<>(TestUtil.aListOf(type));
    }

    public static Pageable aPageable(int size) {
        return Pageable.ofSize(size);
    }

    public static Pageable aPageable() {
        return aPageable(10);
    }
}
