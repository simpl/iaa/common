package eu.europa.ec.simpl.common.test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Map;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.security.test.context.support.WithSecurityContextFactory;

public class WithMockCustomUserSecurityContextFactory implements WithSecurityContextFactory<WithMockSecurityContext> {

    private final String authorityPrefix;

    public WithMockCustomUserSecurityContextFactory(
            @Value("${spring.security.oauth2.resourceserver.jwt.authority-prefix:ROLE_}") String authorityPrefix) {
        this.authorityPrefix = authorityPrefix;
    }

    @Override
    public SecurityContext createSecurityContext(WithMockSecurityContext annotation) {
        SecurityContext context = SecurityContextHolder.createEmptyContext();

        Instant now = Instant.now();
        Authentication auth = new JwtAuthenticationToken(
                new Jwt(
                        "token",
                        now,
                        now.plus(1, ChronoUnit.HOURS),
                        Map.of(
                                "kid", "kid",
                                "typ", "jwt",
                                "alg", "RS256"),
                        Map.of("typ", "Bearer", "email", annotation.email())),
                Arrays.stream(annotation.roles())
                        .map(role -> authorityPrefix + role)
                        .map(SimpleGrantedAuthority::new)
                        .toList());
        context.setAuthentication(auth);
        return context;
    }
}
