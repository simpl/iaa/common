package eu.europa.ec.simpl.common.test;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

public class ResultCaptor<T> implements Answer<Object> {
    private final Class<T> resultClass;
    private T result = null;

    public ResultCaptor(Class<T> resultClass) {
        this.resultClass = resultClass;
    }

    public T getResult() {
        return result;
    }

    @Override
    public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
        result = resultClass.cast(invocationOnMock.callRealMethod());
        return result;
    }
}
