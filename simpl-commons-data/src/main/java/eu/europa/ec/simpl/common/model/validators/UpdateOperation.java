package eu.europa.ec.simpl.common.model.validators;

import jakarta.validation.groups.Default;

public interface UpdateOperation extends Default {}
