package eu.europa.ec.simpl.common.model.dto.onboarding;

import java.util.UUID;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ParticipantTypeDTO {

    UUID id;

    String value;

    String label;
}
