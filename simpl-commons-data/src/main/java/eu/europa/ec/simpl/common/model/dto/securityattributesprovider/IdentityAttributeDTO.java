package eu.europa.ec.simpl.common.model.dto.securityattributesprovider;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Set;
import java.util.UUID;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class IdentityAttributeDTO {

    private UUID id;

    @NotBlank
    private String code;

    @NotBlank
    private String name;

    private String description;

    @Getter(AccessLevel.NONE)
    @NotNull private Boolean assignableToRoles;

    @Getter(AccessLevel.NONE)
    @NotNull private Boolean enabled;

    private Instant creationTimestamp;

    private Instant updateTimestamp;

    private Set<String> participantTypes;

    @Getter(AccessLevel.NONE)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Boolean used;

    public Boolean isAssignableToRoles() {
        return assignableToRoles;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public Boolean isUsed() {
        return used;
    }
}
