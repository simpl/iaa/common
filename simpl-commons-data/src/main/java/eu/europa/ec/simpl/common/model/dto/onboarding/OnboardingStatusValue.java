package eu.europa.ec.simpl.common.model.dto.onboarding;

public enum OnboardingStatusValue {
    IN_PROGRESS,
    IN_REVIEW,
    REJECTED,
    APPROVED
}
