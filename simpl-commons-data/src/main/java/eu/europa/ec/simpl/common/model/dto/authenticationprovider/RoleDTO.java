package eu.europa.ec.simpl.common.model.dto.authenticationprovider;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class RoleDTO {
    @NotNull private UUID id;

    @NotBlank
    private String name;

    private String description;

    private List<String> assignedIdentityAttributes;
}
