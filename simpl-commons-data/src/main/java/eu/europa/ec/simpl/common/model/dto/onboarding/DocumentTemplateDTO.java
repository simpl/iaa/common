package eu.europa.ec.simpl.common.model.dto.onboarding;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.europa.ec.simpl.common.model.validators.CreateOnboardingRequestOperation;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class DocumentTemplateDTO {

    @NotNull(groups = CreateOnboardingRequestOperation.class) private UUID id;

    @NotBlank
    private String name;

    @NotBlank
    private String description;

    @Getter(value = AccessLevel.NONE)
    @NotNull private Boolean mandatory;

    @NotNull private MimeTypeDTO mimeType;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Instant creationTimestamp;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Instant updateTimestamp;

    public Boolean isMandatory() {
        return mandatory;
    }
}
