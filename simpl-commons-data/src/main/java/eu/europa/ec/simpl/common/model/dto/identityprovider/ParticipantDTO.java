package eu.europa.ec.simpl.common.model.dto.identityprovider;

import jakarta.validation.constraints.NotBlank;
import java.time.Instant;
import java.util.UUID;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ParticipantDTO {

    private UUID id;

    @NotBlank
    private String participantType;

    @NotBlank
    private String organization;

    private Instant creationTimestamp;

    private Instant updateTimestamp;

    private String credentialId;

    private Instant expiryDate;
}
