package eu.europa.ec.simpl.common.exchanges.onboarding;

import eu.europa.ec.simpl.common.filters.OnboardingRequestFilter;
import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingRequestDTO;
import eu.europa.ec.simpl.common.responses.PageResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.UUID;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.HttpExchange;

@Tag(name = "Onboarding Requests API", description = "API for managing onboarding requests")
@HttpExchange("onboarding-request")
public interface OnboardingRequestExchange {

    @GetExchange("{onboardingRequestId}")
    OnboardingRequestDTO getOnboardingRequest(@PathVariable("onboardingRequestId") UUID onboardingRequestId);

    @GetExchange
    PageResponse<OnboardingRequestDTO> search(
            @ParameterObject @ModelAttribute OnboardingRequestFilter filter,
            @PageableDefault(sort = "creationTimestamp") @ParameterObject Pageable pageable);
}
