package eu.europa.ec.simpl.common.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Base64Util {

    public static Long getSizeInBytes(String base64) {
        int paddingCount = getPaddingCount(base64);
        return Math.floorDiv(base64.length() * 3, 4) - (long) paddingCount;
    }

    private static int getPaddingCount(String base64) {
        if (base64.endsWith("==")) {
            return 2;
        } else {
            return base64.endsWith("=") ? 1 : 0;
        }
    }
}
