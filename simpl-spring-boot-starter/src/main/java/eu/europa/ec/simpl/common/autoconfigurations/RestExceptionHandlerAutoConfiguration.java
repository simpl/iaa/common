package eu.europa.ec.simpl.common.autoconfigurations;

import eu.europa.ec.simpl.common.exceptions.RestExceptionHandler;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Import;

@AutoConfiguration
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
@Import(RestExceptionHandler.class)
public class RestExceptionHandlerAutoConfiguration {}
