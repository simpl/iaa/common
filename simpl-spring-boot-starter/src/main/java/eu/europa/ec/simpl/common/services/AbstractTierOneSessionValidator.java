package eu.europa.ec.simpl.common.services;

import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jwt.SignedJWT;
import eu.europa.ec.simpl.common.ephemeralproof.EphemeralProofAbstractParser;
import eu.europa.ec.simpl.common.exceptions.EphemeralProofNotFoundException;
import eu.europa.ec.simpl.common.exceptions.InvalidTierOneSessionException;
import eu.europa.ec.simpl.common.exceptions.TierOneTokenNotFound;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import eu.europa.ec.simpl.common.redis.entity.EphemeralProof;
import eu.europa.ec.simpl.common.utils.JwtUtil;
import java.security.KeyFactory;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.text.ParseException;
import java.util.*;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpHeaders;

@Log4j2
public abstract class AbstractTierOneSessionValidator<T extends EphemeralProofAbstractParser<?>> {

    protected abstract Optional<EphemeralProof> fetchEphemeralProofById(String credentialId);

    protected abstract T getEphemeralProofParser(String rawEphemeralProof);

    public void validate(SignedJWT jwt, String credentialId) {
        Optional.ofNullable(jwt).ifPresentOrElse(token -> validateTierOneJwt(credentialId, token), () -> {
            throw new TierOneTokenNotFound();
        });
    }

    public void validate(HttpHeaders headers, String credentialId) {
        validate(JwtUtil.getBearerToken(headers).map(this::parseToken).orElse(null), credentialId);
    }

    private void validateTierOneJwt(String credentialId, SignedJWT jwt) {
        var ephemeralProof = getEphemeralProof(credentialId);
        var tierOnePublicKey = ephemeralProof.getPublicKeys().getLast();
        var jwtAttributes = Optional.ofNullable(JwtUtil.getListClaim(jwt, "identity_attributes", String.class))
                .map(HashSet::new)
                .orElse(new HashSet<>());
        var ephemeralProofAttributes = new HashSet<>(ephemeralProof.getIdentityAttributes().stream()
                .map(IdentityAttributeDTO::getCode)
                .toList());

        if (!verifyTierOneJwtSignature(jwt, tierOnePublicKey) || !ephemeralProofAttributes.containsAll(jwtAttributes)) {
            log.error("Invalid Tier One session for participant {}", credentialId);
            throw new InvalidTierOneSessionException();
        }
    }

    private T getEphemeralProof(String credentialId) {
        return fetchEphemeralProofById(credentialId)
                .map(EphemeralProof::getContent)
                .map(this::getEphemeralProofParser)
                .orElseThrow(EphemeralProofNotFoundException::new);
    }

    private SignedJWT parseToken(String token) {
        try {
            return SignedJWT.parse(token);
        } catch (ParseException e) {
            throw new InvalidTierOneSessionException();
        }
    }

    @SneakyThrows
    private boolean verifyTierOneJwtSignature(SignedJWT jwt, String key) {
        var decoded = Base64.getDecoder().decode(key);
        var publicKey = (RSAPublicKey) KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(decoded));
        return jwt.verify(new RSASSAVerifier(publicKey));
    }
}
