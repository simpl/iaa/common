package eu.europa.ec.simpl.common.exchanges.mtls;

import eu.europa.ec.simpl.client.feign.annotations.PreflightEphemeralProof;
import eu.europa.ec.simpl.common.model.dto.identityprovider.ParticipantDTO;
import eu.europa.ec.simpl.common.model.dto.identityprovider.ParticipantWithIdentityAttributesDTO;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeWithOwnershipDTO;
import feign.Headers;
import feign.Param;
import feign.RequestLine;
import java.util.List;

public interface AuthorityExchange extends ParticipantExchange {
    @RequestLine("POST /identity-api/mtls/token")
    String token();

    @RequestLine("PATCH /identity-api/mtls/public-key")
    @Headers("Content-Type: text/plain")
    ParticipantDTO sendTierOnePublicKey(String tierOnePublicKey);

    @PreflightEphemeralProof
    @RequestLine("GET /identity-api/mtls/echo")
    ParticipantWithIdentityAttributesDTO echo();

    @PreflightEphemeralProof
    @RequestLine("GET /sap-api/mtls/identity-attribute")
    List<IdentityAttributeWithOwnershipDTO> getIdentityAttributesWithOwnership();

    @PreflightEphemeralProof
    @RequestLine("GET /sap-api/mtls/identity-attribute/{credentialId}")
    List<IdentityAttributeDTO> getIdentityAttributesByCredentialIdInUri(@Param String credentialId);
}
