package eu.europa.ec.simpl.common.autoconfigurations;

import eu.europa.ec.simpl.client.core.suppliers.AuthorizationHeaderSupplier;
import eu.europa.ec.simpl.common.interceptors.TierOneTokenPropagatorInterceptor;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Import;

@AutoConfiguration
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
@ConditionalOnClass(AuthorizationHeaderSupplier.class)
@Import(TierOneTokenPropagatorInterceptor.class)
public class TierOneTokenPropagatorAutoConfiguration {}
