package eu.europa.ec.simpl.common.aspects;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Log4j2
@Aspect
public class LoggingAspect {

    private final LoggingLevels loggingLevels;
    private final Map<Class<?>, Stringifier> stringifiers;

    public LoggingAspect(LoggingLevels loggingLevels, List<Stringifier> stringifiers) {
        this.loggingLevels = loggingLevels;
        this.stringifiers =
                stringifiers.stream().collect(Collectors.toMap(Stringifier::appliesTo, Function.identity()));
    }

    @Pointcut("@within(org.springframework.stereotype.Service)")
    private void serviceMethods() {}

    @Before(value = "serviceMethods()")
    public void logBeforeServiceMethods(JoinPoint joinPoint) {
        var targetClass = joinPoint.getTarget().getClass();
        var targetMethod = joinPoint.getSignature();
        var args = joinPoint.getArgs();
        log.atLevel(loggingLevels.service())
                .log("Invoking service method %s.%s(%s)"
                        .formatted(
                                targetClass.getName(),
                                targetMethod.getName(),
                                Arrays.stream(args).map(stringify()).collect(Collectors.joining(", "))));
    }

    private Function<Object, String> stringify() {
        return arg -> getStringifier(arg).stringify(arg);
    }

    private Stringifier getStringifier(Object arg) {
        return Optional.ofNullable(stringifiers.get(arg.getClass())).orElse(Stringifier.Default);
    }

    public static class Stringifier {

        private final Class<?> clazz;
        private final Function<Object, String> toString;

        public static <T> Stringifier create(Class<T> clazz, Function<T, String> toString) {
            return new Stringifier(clazz, o -> o.getClass() == clazz ? toString.apply(clazz.cast(o)) : o.toString());
        }

        private Stringifier(Class<?> clazz, Function<Object, String> toString) {
            this.clazz = clazz;
            this.toString = toString;
        }

        public String stringify(Object object) {
            return toString.apply(object);
        }

        public Class<?> appliesTo() {
            return clazz;
        }

        public static final Stringifier Default = new Stringifier(Object.class, Object::toString);
    }

    public record LoggingLevels(Level service) {}
}
