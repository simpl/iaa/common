package eu.europa.ec.simpl.common.csr;

public interface AlgorithmConfig {
    String signatureAlgorithm();

    String algorithm();

    int keyLength();
}
