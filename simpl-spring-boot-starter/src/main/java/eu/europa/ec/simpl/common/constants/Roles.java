package eu.europa.ec.simpl.common.constants;

public class Roles {

    private Roles() {}

    // Onboarding manager
    public static final String ONBOARDER_M = "ONBOARDER_M";
    // Tier 2 identity attributes manager
    public static final String IATTR_M = "IATTR_M";
    // Tier 1 user and roles manager
    public static final String T1UAR_M = "T1UAR_M";
    // Tier 2 authorization manager
    public static final String T2IAA_M = "T2IAA_M";
    // Tier 1 authorization manager
    public static final String NOTARY = "NOTARY";
    public static final String APPLICANT = "APPLICANT";
}
