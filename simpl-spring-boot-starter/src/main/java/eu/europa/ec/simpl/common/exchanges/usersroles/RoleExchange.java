package eu.europa.ec.simpl.common.exchanges.usersroles;

import eu.europa.ec.simpl.common.filters.RoleFilter;
import eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakRoleDTO;
import eu.europa.ec.simpl.common.model.dto.usersroles.RoleDTO;
import eu.europa.ec.simpl.common.responses.PageResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import java.util.List;
import java.util.UUID;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.service.annotation.*;

@Tag(name = "Role API", description = "API for managing roles")
@HttpExchange("role")
public interface RoleExchange {

    @GetExchange("/{id}")
    @Operation(
            summary = "Find role by ID",
            description = "Retrieves a role by its unique identifier (UUID)",
            parameters = {
                @Parameter(
                        name = "id",
                        description = "The UUID of the role",
                        required = true,
                        example = "123e4567-e89b-12d3-a456-426614174000",
                        schema = @Schema(type = "string", format = "uuid"))
            },
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Successfully retrieved the role",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = RoleDTO.class))),
                @ApiResponse(responseCode = "400", description = "Invalid UUID format"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
                @ApiResponse(responseCode = "404", description = "Role not found"),
            })
    RoleDTO findById(@PathVariable("id") UUID roleId);

    @PostExchange
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(
            summary = "Create a new role",
            description = "Creates a new role in the system with the provided details",
            requestBody =
                    @io.swagger.v3.oas.annotations.parameters.RequestBody(
                            description = "The details of the role to be created",
                            required = true,
                            content = @Content(schema = @Schema(implementation = KeycloakRoleDTO.class))),
            responses = {
                @ApiResponse(
                        responseCode = "201",
                        description = "Successfully created the role",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = RoleDTO.class))),
                @ApiResponse(responseCode = "400", description = "Invalid input data"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
                @ApiResponse(responseCode = "409", description = "Conflict: role already exists"),
            })
    RoleDTO create(@RequestBody @Valid KeycloakRoleDTO roleDTO);

    @PutExchange
    @Operation(
            summary = "Update an existing role",
            description = "Updates the details of an existing role in the system",
            requestBody =
                    @io.swagger.v3.oas.annotations.parameters.RequestBody(
                            description = "The details of the role to be updated",
                            required = true,
                            content = @Content(schema = @Schema(implementation = KeycloakRoleDTO.class))),
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Successfully updated the role",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = RoleDTO.class))),
                @ApiResponse(responseCode = "400", description = "Invalid input data"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(
                        responseCode = "403",
                        description =
                                "Forbidden: Cannot modify role's name or User " + "does not have the required role"),
                @ApiResponse(responseCode = "404", description = "Role not found"),
            })
    RoleDTO update(@RequestBody @Valid RoleDTO roleDTO);

    @PutExchange("/{id}/identity-attributes")
    @Operation(
            summary = "Assign identity attributes to a role",
            description = "Assigns a list of identity attribute IDs to a specified role",
            parameters = {
                @Parameter(
                        name = "id",
                        description = "The UUID of the role to which identity attributes will be assigned",
                        required = true,
                        example = "123e4567-e89b-12d3-a456-426614174000",
                        schema = @Schema(type = "string", format = "uuid"))
            },
            requestBody =
                    @io.swagger.v3.oas.annotations.parameters.RequestBody(
                            description = "List of identity attribute codes to be assigned to the role",
                            required = true,
                            content = @Content(array = @ArraySchema(schema = @Schema(type = "string")))),
            responses = {
                @ApiResponse(responseCode = "200", description = "Successfully assigned identity attributes"),
                @ApiResponse(responseCode = "400", description = "Invalid input data"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
                @ApiResponse(responseCode = "404", description = "Role not found"),
            })
    void replaceIdentityAttributes(@PathVariable("id") UUID roleId, @RequestBody List<String> identityAttributesCodes);

    @DeleteExchange("/delete-attribute")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(
            summary = "Delete an identity attribute from a role",
            description = "Removes an identity attribute from a specified role",
            parameters = {
                @Parameter(
                        name = "roleId",
                        description = "The UUID of the role from which the attribute will be removed",
                        required = true,
                        schema = @Schema(type = "string", format = "uuid")),
                @Parameter(
                        name = "attributeCode",
                        description = "The code of the attribute to be removed",
                        required = true,
                        schema = @Schema(type = "string"))
            },
            responses = {
                @ApiResponse(responseCode = "204", description = "Successfully removed the attribute from the role"),
                @ApiResponse(responseCode = "400", description = "Invalid input data"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
            })
    void deleteAttributeFromRole(@RequestParam UUID roleId, @RequestParam String attributeCode);

    @DeleteExchange("/{roleId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(
            summary = "Delete a role by id",
            description = "Removes a role from the system using its id",
            parameters = {
                @Parameter(
                        name = "roleId",
                        description = "The name of the role to be deleted",
                        required = true,
                        schema = @Schema(type = "string", format = "uuid"))
            },
            responses = {
                @ApiResponse(responseCode = "204", description = "Successfully deleted the role"),
                @ApiResponse(responseCode = "400", description = "Invalid role name"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
                @ApiResponse(responseCode = "404", description = "Role not found"),
            })
    void delete(@PathVariable("roleId") UUID roleId);

    @GetExchange("/search")
    @Operation(
            summary = "Search roles",
            description = "Searches for roles based on the provided filter criteria and pagination settings",
            responses = {
                @ApiResponse(responseCode = "200", description = "Successfully retrieved the list of roles"),
                @ApiResponse(responseCode = "400", description = "Invalid filter or pagination parameters"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
            })
    PageResponse<RoleDTO> search(
            @ParameterObject RoleFilter filter, @ParameterObject @PageableDefault(sort = "id") Pageable pageable);

    @PostExchange(value = "/{id}/duplicate-identity-attribute")
    @Operation(
            summary = "Duplicate identity attributes to another role",
            description = "Duplicates identity attributes from the source role to the destination role",
            parameters = {
                @Parameter(
                        name = "id",
                        description = "The UUID of the source role",
                        required = true,
                        schema = @Schema(type = "string", format = "uuid"))
            },
            requestBody =
                    @io.swagger.v3.oas.annotations.parameters.RequestBody(
                            description = "The UUID of the destination role",
                            required = true,
                            content = @Content(schema = @Schema(type = "string", format = "uuid"))),
            responses = {
                @ApiResponse(responseCode = "200", description = "Successfully duplicated identity attributes"),
                @ApiResponse(responseCode = "400", description = "Invalid input data"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
                @ApiResponse(responseCode = "404", description = "Source or destination role not found"),
            })
    void duplicateIdentityAttributeToAnOtherRole(
            @PathVariable("id") UUID sourceRoleId, @RequestBody String destinationRoleId);

    @PostExchange("assigned-identity-attributes")
    @Operation(
            summary = "Get identity attributes from role list",
            description = "Retrieves a list of identity attributes associated with the provided roles",
            requestBody =
                    @io.swagger.v3.oas.annotations.parameters.RequestBody(
                            description = "List of role names from which to retrieve identity attributes",
                            required = true,
                            content = @Content(array = @ArraySchema(schema = @Schema(type = "string")))),
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Successfully retrieved identity attributes",
                        content = @Content(array = @ArraySchema(schema = @Schema(type = "string")))),
                @ApiResponse(responseCode = "400", description = "Invalid input data"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
            })
    List<String> getIdentityAttributesFromRoleList(@RequestBody List<String> roleNameList);

    @ResponseStatus(HttpStatus.CREATED)
    @PostExchange("import")
    void importRoles(@Valid @RequestBody List<KeycloakRoleDTO> roles);
}
