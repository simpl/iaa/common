package eu.europa.ec.simpl.common.autoconfigurations;

import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.PropertySource;

@AutoConfiguration
@ConditionalOnWebApplication
@PropertySource("classpath:/properties/hibernate.properties")
public class HibernatePropertiesAutoConfiguration {}
