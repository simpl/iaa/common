package eu.europa.ec.simpl.common.security;

import com.nimbusds.jwt.SignedJWT;
import eu.europa.ec.simpl.common.exceptions.JwtNotFoundException;
import eu.europa.ec.simpl.common.utils.JwtUtil;
import jakarta.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;

@Log4j2
@Service
public class JwtService implements AuthService {

    private final HttpServletRequest request;

    public JwtService(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    @SneakyThrows
    public UUID getUserId() {
        return UUID.fromString(getJwt().getJWTClaimsSet().getSubject());
    }

    @Override
    @SneakyThrows
    public String getEmail() {
        return getJwt().getJWTClaimsSet().getStringClaim("email");
    }

    @Override
    @SneakyThrows
    public String getUsername() {
        return getJwt().getJWTClaimsSet().getStringClaim("preferred_username");
    }

    @Override
    @SneakyThrows
    public String getClaim(String claim) {
        return getJwt().getJWTClaimsSet().getStringClaim(claim);
    }

    @Override
    public String getKid() {
        return getJwt().getHeader().getKeyID();
    }

    @Override
    @SneakyThrows
    public List<String> getIdentityAttributes() {
        return Optional.ofNullable(getJwt().getJWTClaimsSet().getStringListClaim("identity_attributes"))
                .orElseGet(() -> {
                    log.error("No identity_attributes claim found in JWT");
                    return List.of();
                });
    }

    private SignedJWT getJwt() {
        var bearerToken = JwtUtil.getBearerToken(() -> request.getHeader(HttpHeaders.AUTHORIZATION))
                .orElseThrow(JwtNotFoundException::new);
        return JwtUtil.parseJwt(bearerToken);
    }

    @Override
    public boolean isAuthenticated() {
        if (RequestContextHolder.getRequestAttributes() != null) {
            return JwtUtil.getBearerToken(() -> request.getHeader(HttpHeaders.AUTHORIZATION))
                    .isPresent();
        } else {
            return false;
        }
    }

    @Override
    @SneakyThrows
    public boolean hasRole(String role) {
        return Optional.ofNullable(getJwt().getJWTClaimsSet().getListClaim("client-roles"))
                .map(list -> list.contains(role))
                .orElseGet(() -> {
                    log.error("No client-roles claim {} found in JWT", role);
                    return false;
                });
    }
}
