package eu.europa.ec.simpl.common.exceptions;

public class CSRException extends RuntimeException {
    public CSRException(String message, Throwable cause) {
        super(message, cause);
    }
}
