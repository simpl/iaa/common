package eu.europa.ec.simpl.common.exceptions;

import org.springframework.http.HttpStatus;

public class InvalidEphemeralProofException extends StatusException {
    public InvalidEphemeralProofException() {
        this(HttpStatus.UNAUTHORIZED, "Invalid Ephemeral Proof");
    }

    protected InvalidEphemeralProofException(HttpStatus status, String message) {
        super(status, message);
    }
}
