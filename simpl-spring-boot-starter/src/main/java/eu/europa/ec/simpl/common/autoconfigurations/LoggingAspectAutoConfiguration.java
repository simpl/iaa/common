package eu.europa.ec.simpl.common.autoconfigurations;

import eu.europa.ec.simpl.common.aspects.LoggingAspect;
import java.util.List;
import org.apache.logging.log4j.Level;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;

@AutoConfiguration
public class LoggingAspectAutoConfiguration {

    @Bean
    @ConditionalOnProperty(prefix = "logging.aspect", name = "enabled", havingValue = "true")
    public LoggingAspect loggingAspect(
            @Value("${logging.service.level:INFO}") Level serviceLoggingLevel,
            List<LoggingAspect.Stringifier> customStringifiers) {
        return new LoggingAspect(new LoggingAspect.LoggingLevels(serviceLoggingLevel), customStringifiers);
    }
}
