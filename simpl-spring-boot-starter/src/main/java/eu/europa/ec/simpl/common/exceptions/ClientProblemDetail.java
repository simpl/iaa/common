package eu.europa.ec.simpl.common.exceptions;

import static eu.europa.ec.simpl.common.exceptions.SimplErrors.*;

import java.net.URI;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;

public class ClientProblemDetail extends ProblemDetail {

    private final ProblemDetail cause;

    public ClientProblemDetail(ProblemDetail cause) {
        this.cause = cause;
    }

    @Override
    public URI getType() {
        return CLIENT_ERROR_TYPE;
    }

    @Override
    public String getTitle() {
        return "Client error";
    }

    @Override
    public String getDetail() {
        return "Client invocation returned an error";
    }

    @Override
    public int getStatus() {
        return HttpStatus.INTERNAL_SERVER_ERROR.value();
    }

    public ProblemDetail getCause() {
        return cause;
    }
}
