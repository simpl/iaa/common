package eu.europa.ec.simpl.common.filters;

import java.time.Instant;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class OnboardingRequestFilter {
    private String status;
    private String email;
    private Instant creationTimestampFrom;
    private Instant creationTimestampTo;
    private Instant updateTimestampFrom;
    private Instant updateTimestampTo;
    private Instant lastStatusUpdateTimestampFrom;
    private Instant lastStatusUpdateTimestampTo;
}
