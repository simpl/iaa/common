package eu.europa.ec.simpl.common.autoconfigurations;

import eu.europa.ec.simpl.common.security.JwtService;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Import;

@AutoConfiguration
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
@Import(JwtService.class)
public class AuthServiceAutoConfiguration {}
