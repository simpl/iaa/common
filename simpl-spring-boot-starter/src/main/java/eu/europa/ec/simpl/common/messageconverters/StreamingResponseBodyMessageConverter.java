package eu.europa.ec.simpl.common.messageconverters;

import java.io.IOException;
import java.util.Objects;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

public class StreamingResponseBodyMessageConverter extends AbstractHttpMessageConverter<StreamingResponseBody> {

    public StreamingResponseBodyMessageConverter() {
        super(MediaType.APPLICATION_OCTET_STREAM, MediaType.ALL);
    }

    @Override
    protected boolean supports(Class<?> clazz) {
        return Objects.equals(StreamingResponseBody.class, clazz);
    }

    @Override
    protected StreamingResponseBody readInternal(
            Class<? extends StreamingResponseBody> clazz, HttpInputMessage inputMessage)
            throws IOException, HttpMessageNotReadableException {
        var message = inputMessage.getBody().readAllBytes();
        return outputStream -> outputStream.write(message);
    }

    @Override
    protected void writeInternal(StreamingResponseBody streamingResponseBody, HttpOutputMessage outputMessage)
            throws IOException, HttpMessageNotWritableException {
        streamingResponseBody.writeTo(outputMessage.getBody());
    }
}
