package eu.europa.ec.simpl.common.requests;

import eu.europa.ec.simpl.common.model.dto.authenticationprovider.DistinguishedNameDTO;
import jakarta.validation.Valid;
import java.util.List;
import java.util.UUID;
import org.springframework.validation.annotation.Validated;

@Validated
public record InitializeAuthorityRequest(
        @Valid DistinguishedNameDTO distinguishedName, List<UUID> identityAttributes) {}
