package eu.europa.ec.simpl.common.utils;

import eu.europa.ec.simpl.common.exceptions.RuntimeWrapperException;
import java.io.*;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.List;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import lombok.extern.log4j.Log4j2;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;

@Log4j2
@UtilityClass
public class PemUtil {

    public static void writePem(List<X509Certificate> certificateList, OutputStream outputStream) {
        try (var pemWriter = new JcaPEMWriter(new OutputStreamWriter(outputStream))) {
            for (var x509Certificate : certificateList) {
                pemWriter.writeObject(x509Certificate);
            }
        } catch (IOException e) {
            log.error("error writing pem", e);
            throw new RuntimeWrapperException(e);
        }
    }

    public static List<InputStream> loadPemObjects(InputStream inputStream) {
        List<InputStream> pemStreams = new ArrayList<>();
        try (var pemReader = new PemReader(new InputStreamReader(inputStream))) {
            PemObject pemObject;
            while ((pemObject = pemReader.readPemObject()) != null) {
                pemStreams.add(new ByteArrayInputStream(pemObject.getContent()));
            }
        } catch (IOException e) {
            log.error("error reading PEM", e);
            throw new RuntimeWrapperException(e);
        }
        return pemStreams;
    }

    @SneakyThrows
    public static PrivateKey loadPrivateKey(InputStream inputStream, String algorithm) {
        var pemContent = PemUtil.loadPemObjects(inputStream).getFirst().readAllBytes();
        var keySpec = new PKCS8EncodedKeySpec(pemContent);
        var keyFactory = KeyFactory.getInstance(algorithm);
        return keyFactory.generatePrivate(keySpec);
    }
}
