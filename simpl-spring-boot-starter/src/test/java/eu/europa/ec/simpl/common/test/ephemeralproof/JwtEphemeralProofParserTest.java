package eu.europa.ec.simpl.common.test.ephemeralproof;

import static org.assertj.core.api.Assertions.assertThat;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jwt.SignedJWT;
import eu.europa.ec.simpl.common.ephemeralproof.JwtEphemeralProofParser;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import eu.europa.ec.simpl.common.utils.JwtUtil;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.util.List;
import java.util.Map;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.instancio.junit.InstancioSource;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;

class JwtEphemeralProofParserTest {

    @BeforeAll
    public static void setup() {
        Security.addProvider(new BouncyCastleProvider());
    }

    @InstancioSource
    @ParameterizedTest
    void getIdentityAttributes(List<IdentityAttributeDTO> expectedIdentityAttributes)
            throws NoSuchAlgorithmException, JOSEException {

        var jwt = getSignedJWT(expectedIdentityAttributes);

        var ep = new JwtEphemeralProofParser(jwt.serialize());

        assertThat(ep.getIdentityAttributes()).isEqualTo(expectedIdentityAttributes);
    }

    private SignedJWT getSignedJWT(List<IdentityAttributeDTO> expectedIdentityAttributes)
            throws NoSuchAlgorithmException, JOSEException {
        return JwtUtil.createSignedJWT(Map.of("attributes", expectedIdentityAttributes));
    }
}
