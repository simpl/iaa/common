package eu.europa.ec.simpl.common.argumentresolvers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.MethodParameter;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.service.invoker.HttpRequestValues;

@ExtendWith(MockitoExtension.class)
public class PageableArgumentResolverTest {

    @Mock
    private MethodParameter parameter;

    @Mock
    private HttpRequestValues.Builder requestValues;

    @Test
    public void resolve_when_thenReturnFalse() {
        var pageableArgumentResolver = new PageableArgumentResolver();
        var pageable = mock(Pageable.class);
        var argument = pageable;
        when(parameter.getParameterType()).thenReturn((Class) Object.class);
        var result = pageableArgumentResolver.resolve(argument, parameter, requestValues);
        assertThat(result).isFalse();
    }

    @Test
    public void resolve_when_thenReturnTrue() {
        var pageableArgumentResolver = new PageableArgumentResolver();
        var pageable = mock(Pageable.class);
        var argument = pageable;
        var sort = mock(Sort.class);
        var order = mock(Sort.Order.class);
        var direction = mock(Direction.class);
        var orderList = List.of(order);
        when(pageable.getSort()).thenReturn(sort);
        when(pageable.isPaged()).thenReturn(true);
        when(parameter.getParameterType()).thenReturn((Class) Pageable.class);
        when(sort.isSorted()).thenReturn(true);
        when(sort.iterator()).thenReturn(orderList.iterator());
        when(order.getDirection()).thenReturn(direction);
        when(direction.name()).thenReturn("junit-direction-name");

        var result = pageableArgumentResolver.resolve(argument, parameter, requestValues);
        assertThat(result).isTrue();
    }
}
