package eu.europa.ec.simpl.common.logging;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayInputStream;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.boot.context.properties.source.ConfigurationPropertySource;
import org.springframework.boot.context.properties.source.MapConfigurationPropertySource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.mock.http.server.reactive.MockServerHttpRequest;
import org.springframework.mock.web.server.MockServerWebExchange;

@ExtendWith(MockitoExtension.class)
class LoggingRuleTest {

    @Test
    void matches_withPathAndMethod_shouldMatchPathAndMethod() {

        var config = loadRouteConfig(
                """
        routes:
          logging:
            business:
              - method: GET
                path: /some/path
                config:
                  message: My message
                  operations:
                    - OP1
                    - OP2
        """);

        var request = MockServerHttpRequest.get("/some/path");
        var exchange = MockServerWebExchange.from(request);
        assertThat(config.logging().business().getFirst().matches(exchange).block())
                .isTrue();
    }

    @Test
    void matches_withPathAndMethod_shouldNotMatchDifferentPath() {

        var config = loadRouteConfig(
                """
        routes:
          logging:
            business:
              - method: GET
                path: /some/path
                config:
                  message: My message
                  operations:
                    - OP1
                    - OP2
        """);

        var request = MockServerHttpRequest.get("/some/other/path");
        var exchange = MockServerWebExchange.from(request);
        assertThat(config.logging().business().getFirst().matches(exchange).block())
                .isFalse();
    }

    @Test
    void matches_withPathAndMethod_shouldNotMatchDifferentMethod() {

        var config = loadRouteConfig(
                """
        routes:
          logging:
            business:
              - method: GET
                path: /some/path
                config:
                  message: My message
                  operations:
                    - OP1
                    - OP2
        """);

        var request = MockServerHttpRequest.post("/some/path");
        var exchange = MockServerWebExchange.from(request);
        assertThat(config.logging().business().getFirst().matches(exchange).block())
                .isFalse();
    }

    @Test
    void matches_withQueryParamName_shouldMatchQueryParamNameWithAnyValue() {

        var config = loadRouteConfig(
                """
        routes:
          logging:
            business:
              - method: GET
                path: /some/path
                query:
                  - name: query1
                config:
                  message: My message
                  operations:
                    - OP1
                    - OP2
        """);

        var request = MockServerHttpRequest.get("/some/path").queryParam("query1", "anyValue");
        var exchange = MockServerWebExchange.from(request);
        assertThat(config.logging().business().getFirst().matches(exchange).block())
                .isTrue();
    }

    @Test
    void matches_withQueryParamNameAndValue_shouldMatchQueryParamNameAndValue() {

        var config = loadRouteConfig(
                """
        routes:
          logging:
            business:
              - method: GET
                path: /some/path
                query:
                  - name: query1
                    value: value1
                config:
                  message: My message
                  operations:
                    - OP1
                    - OP2
        """);

        var request = MockServerHttpRequest.get("/some/path").queryParam("query1", "value1");
        var exchange = MockServerWebExchange.from(request);
        assertThat(config.logging().business().getFirst().matches(exchange).block())
                .isTrue();
    }

    @Test
    void matches_withQueryParamNameAndValue_shouldNotMatchQueryParamNameWithDifferentValue() {

        var config = loadRouteConfig(
                """
        routes:
          logging:
            business:
              - method: GET
                path: /some/path
                query:
                  - name: query1
                    value: value1
                config:
                  message: My message
                  operations:
                    - OP1
                    - OP2
        """);

        var request = MockServerHttpRequest.get("/some/path").queryParam("query1", "otherValue");
        var exchange = MockServerWebExchange.from(request);
        assertThat(config.logging().business().getFirst().matches(exchange).block())
                .isFalse();
    }

    @Test
    void matches_withMultipleQueryParams_shouldMatchQueryParams() {
        var config = loadRouteConfig(
                """
        routes:
          logging:
            business:
              - method: GET
                path: /some/path
                query:
                  - name: query1
                    value: value1
                  - name: query2
                    value: value2
                  - name: query3
                config:
                  message: My message
                  operations:
                    - OP1
                    - OP2
        """);

        var request = MockServerHttpRequest.get("/some/path")
                .queryParam("query1", "value1")
                .queryParam("query2", "value2")
                .queryParam("query3", "anyValue");
        var exchange = MockServerWebExchange.from(request);
        assertThat(config.logging().business().getFirst().matches(exchange).block())
                .isTrue();
    }

    @Test
    void matches_withMultipleQueryParamValues_shouldMatchAnyValue() {
        var config = loadRouteConfig(
                """
        routes:
          logging:
            business:
              - method: GET
                path: /some/path
                query:
                  - name: query1
                    values:
                    - value1
                    - value2
                config:
                  message: My message
                  operations:
                    - OP1
                    - OP2
        """);

        var request = MockServerHttpRequest.get("/some/path").queryParam("query1", "value1");
        var exchange = MockServerWebExchange.from(request);
        assertThat(config.logging().business().getFirst().matches(exchange).block())
                .isTrue();

        request = MockServerHttpRequest.get("/some/path").queryParam("query1", "value2");
        exchange = MockServerWebExchange.from(request);
        assertThat(config.logging().business().getFirst().matches(exchange).block())
                .isTrue();
    }

    @Test
    void matches_withHeaderName_shouldMatchHeaderNameWithAnyValue() {

        var config = loadRouteConfig(
                """
        routes:
          logging:
            business:
              - method: GET
                path: /some/path
                header:
                  - name: header1
                config:
                  message: My message
                  operations:
                    - OP1
                    - OP2
        """);

        var request = MockServerHttpRequest.get("/some/path").header("header1", "anyValue");
        var exchange = MockServerWebExchange.from(request);
        assertThat(config.logging().business().getFirst().matches(exchange).block())
                .isTrue();
    }

    @Test
    void matches_withHeaderNameAndValue_shouldMatchHeaderNameAndValue() {

        var config = loadRouteConfig(
                """
        routes:
          logging:
            business:
              - method: GET
                path: /some/path
                header:
                  - name: header1
                    value: value1
                config:
                  message: My message
                  operations:
                    - OP1
                    - OP2
        """);

        var request = MockServerHttpRequest.get("/some/path").header("header1", "value1");
        var exchange = MockServerWebExchange.from(request);
        assertThat(config.logging().business().getFirst().matches(exchange).block())
                .isTrue();
    }

    @Test
    void matches_withHeaderNameAndValue_shouldNotMatchHeaderWithDifferentValue() {

        var config = loadRouteConfig(
                """
        routes:
          logging:
            business:
              - method: GET
                path: /some/path
                header:
                  - name: header1
                    value: value1
                config:
                  message: My message
                  operations:
                    - OP1
                    - OP2
        """);

        var request = MockServerHttpRequest.get("/some/path").queryParam("header1", "otherValue");
        var exchange = MockServerWebExchange.from(request);
        assertThat(config.logging().business().getFirst().matches(exchange).block())
                .isFalse();
    }

    @Test
    void matches_withMultipleHeaders_shouldMatchHeaders() {
        var config = loadRouteConfig(
                """
        routes:
          logging:
            business:
              - method: GET
                path: /some/path
                header:
                  - name: header1
                    value: value1
                  - name: header2
                    value: value2
                  - name: header3
                config:
                  message: My message
                  operations:
                    - OP1
                    - OP2
        """);

        var request = MockServerHttpRequest.get("/some/path")
                .header("header1", "value1")
                .header("header2", "value2")
                .header("header3", "anyValue");
        var exchange = MockServerWebExchange.from(request);
        assertThat(config.logging().business().getFirst().matches(exchange).block())
                .isTrue();
    }

    @Test
    void matches_withMultipleHeaderValues_shouldMatchAnyValue() {
        var config = loadRouteConfig(
                """
        routes:
          logging:
            business:
              - method: GET
                path: /some/path
                header:
                  - name: header1
                    values:
                    - value1
                    - value2
                config:
                  message: My message
                  operations:
                    - OP1
                    - OP2
        """);

        var request = MockServerHttpRequest.get("/some/path").header("header1", "value1");
        var exchange = MockServerWebExchange.from(request);
        assertThat(config.logging().business().getFirst().matches(exchange).block())
                .isTrue();

        request = MockServerHttpRequest.get("/some/path").header("header1", "value2");
        exchange = MockServerWebExchange.from(request);
        assertThat(config.logging().business().getFirst().matches(exchange).block())
                .isTrue();
    }

    static RouteConfig loadRouteConfig(String config) {
        YamlPropertiesFactoryBean yamlPropertiesFactoryBean = new YamlPropertiesFactoryBean();
        yamlPropertiesFactoryBean.setResources(new InputStreamResource(new ByteArrayInputStream(config.getBytes())));
        ConfigurationPropertySource propertySource =
                new MapConfigurationPropertySource(yamlPropertiesFactoryBean.getObject());
        Binder binder = new Binder(propertySource);
        return binder.bind("routes", RouteConfig.class).get();
    }

    public record RouteConfig(Logging logging) {

        public record Logging(List<LoggingRule> business) {}
    }
}
