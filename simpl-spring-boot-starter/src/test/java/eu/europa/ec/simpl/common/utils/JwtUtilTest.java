package eu.europa.ec.simpl.common.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import eu.europa.ec.simpl.common.exceptions.RuntimeWrapperException;
import java.text.ParseException;
import java.util.Date;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;

class JwtUtilTest {

    @Test
    void parseJwt_shouldParseValidJwt() {
        String validJwt =
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

        SignedJWT result = JwtUtil.parseJwt(validJwt);

        assertThat(result).isNotNull();
        assertThat(result.getHeader().getAlgorithm().getName()).isEqualTo("HS256");
    }

    @Test
    void parseJwt_shouldThrowRuntimeWrapperException_whenInvalidJwt() {
        var invalidJwt = "invalid.jwt.token";

        assertThatThrownBy(() -> JwtUtil.parseJwt(invalidJwt))
                .isInstanceOf(RuntimeWrapperException.class)
                .hasCauseInstanceOf(ParseException.class);
    }

    @Test
    void getBearerToken_withSupplier_shouldReturnToken_whenValidBearerToken() {
        var validHeader = "Bearer token123";

        var result = JwtUtil.getBearerToken(() -> validHeader);

        assertThat(result).isPresent().contains("token123");
    }

    @Test
    void getBearerToken_withSupplier_shouldReturnEmpty_whenNoBearerToken() {
        var invalidHeader = "Basic dXNlcjpwYXNzd29yZA==";

        var result = JwtUtil.getBearerToken(() -> invalidHeader);

        assertThat(result).isEmpty();
    }

    @Test
    void getBearerToken_withHttpHeaders_shouldReturnToken_whenValidBearerToken() {
        var headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, "Bearer token456");

        var result = JwtUtil.getBearerToken(headers);

        assertThat(result).isPresent().contains("token456");
    }

    @Test
    void getBearerToken_withHttpHeaders_shouldReturnEmpty_whenNoAuthorizationHeader() {
        var headers = new HttpHeaders();

        var result = JwtUtil.getBearerToken(headers);

        assertThat(result).isEmpty();
    }

    @Test
    void getClaimSet_shouldReturnClaimSet_whenValidJwt() throws Exception {
        var jwt = mock(SignedJWT.class);
        var claimsSet = new JWTClaimsSet.Builder()
                .subject("1234567890")
                .issuer("test")
                .expirationTime(new Date(1516239022000L))
                .build();
        when(jwt.getJWTClaimsSet()).thenReturn(claimsSet);

        var result = JwtUtil.getClaimSet(jwt);

        assertThat(result).isNotNull();
        assertThat(result.getSubject()).isEqualTo("1234567890");
        assertThat(result.getIssuer()).isEqualTo("test");
        assertThat(result.getExpirationTime()).isEqualTo(new Date(1516239022000L));
    }

    @Test
    void getClaimSet_shouldThrowRuntimeWrapperException_whenParseExceptionOccurs() throws Exception {
        var jwt = mock(SignedJWT.class);
        when(jwt.getJWTClaimsSet()).thenThrow(new ParseException("Error parsing JWT", 0));

        assertThatThrownBy(() -> JwtUtil.getClaimSet(jwt))
                .isInstanceOf(RuntimeWrapperException.class)
                .hasCauseInstanceOf(ParseException.class);
    }
}
