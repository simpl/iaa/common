package eu.europa.ec.simpl.common.messageconverters;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

public class StreamingResponseBodyMessageConverterTest {

    @Test
    public void readInternal_whenStreamingResponseBody_thenWriteStream() throws Exception {
        var converter = new StreamingResponseBodyMessageConverter();
        var inputMessage = mock(HttpInputMessage.class);
        var in = new ByteArrayInputStream("junit-test".getBytes());
        when(inputMessage.getBody()).thenReturn(in);
        var result = converter.readInternal(null, inputMessage);
        var out = new ByteArrayOutputStream();
        result.writeTo(out);
        assertThat(new String(out.toByteArray())).isEqualTo("junit-test");
    }

    @Test
    public void writeInternal_whenStreamingResponseBody() {
        var converter = new StreamingResponseBodyMessageConverter();
        var streamingResponseBody = mock(StreamingResponseBody.class);
        var outputMessage = mock(HttpOutputMessage.class);
        assertDoesNotThrow(() -> converter.writeInternal(streamingResponseBody, outputMessage));
    }

    @Test
    public void supports_whenStreamingResponseBody_thenReturnTrue() {
        var converter = new StreamingResponseBodyMessageConverter();
        var result = converter.supports(StreamingResponseBody.class);
        assertThat(result).isTrue();
    }
}
