package eu.europa.ec.simpl.common.exceptions;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class TestModel {
    @NotBlank
    String content;

    @NotNull Integer value;
}
