package eu.europa.ec.simpl.common.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

import eu.europa.ec.simpl.common.exceptions.RuntimeWrapperException;
import eu.europa.ec.simpl.common.test.util.TestCertificateUtil;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.util.Collections;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class CredentialUtilTest {

    @Mock
    private KeyStore mockKeyStore;

    @Mock
    private X509Certificate mockCertificate;

    @Mock
    private ECPublicKey mockPublicKey;

    @BeforeEach
    void setUp() throws Exception {
        Security.addProvider(new BouncyCastleProvider());
        when(mockKeyStore.aliases()).thenReturn(Collections.enumeration(Collections.singletonList("alias")));
        when(mockKeyStore.getCertificateChain("alias")).thenReturn(new Certificate[] {mockCertificate});
    }

    @Test
    void extractCertificateFromKeystore_shouldReturnCertificate() {
        X509Certificate result = CredentialUtil.extractCertificateFromKeystore(mockKeyStore);
        assertThat(result).isEqualTo(mockCertificate);
    }

    @Test
    void extractPublicKeyFromKeystore_shouldReturnECPublicKey() {
        when(mockCertificate.getPublicKey()).thenReturn(mockPublicKey);

        ECPublicKey result = CredentialUtil.extractPublicKeyFromKeystore(mockKeyStore);
        assertThat(result).isEqualTo(mockPublicKey);
    }

    @Test
    void extractPrivateKeyFromKeystore_shouldReturnECPrivateKey() throws Exception {
        ECPrivateKey mockPrivateKey = mock(ECPrivateKey.class);
        when(mockKeyStore.getKey(eq("alias"), any(char[].class))).thenReturn(mockPrivateKey);

        ECPrivateKey result = CredentialUtil.extractPrivateKeyFromKeystore(mockKeyStore, "password");
        assertThat(result).isEqualTo(mockPrivateKey);
    }

    @Test
    void getIdentifierFromSubject_shouldReturnIdentifier() {
        try (MockedStatic<CredentialUtil> mockedStatic = mockStatic(CredentialUtil.class)) {
            mockedStatic
                    .when(() -> CredentialUtil.getIdentifierFromSubject(any(X509Certificate.class), eq(BCStyle.O)))
                    .thenReturn("TestOrg");

            String result = CredentialUtil.getIdentifierFromSubject(mockCertificate, BCStyle.O);
            assertThat(result).isEqualTo("TestOrg");
        }
    }

    @Test
    void loadPrivateKey_shouldReturnPrivateKey() throws Exception {
        byte[] encoded = "testkey".getBytes();
        String algorithm = "EC";

        KeyFactory mockKeyFactory = mock(KeyFactory.class);
        PrivateKey mockPrivateKey = mock(PrivateKey.class);

        try (MockedStatic<KeyFactory> mockedStatic = mockStatic(KeyFactory.class)) {
            mockedStatic.when(() -> KeyFactory.getInstance(algorithm)).thenReturn(mockKeyFactory);
            when(mockKeyFactory.generatePrivate(any())).thenReturn(mockPrivateKey);

            PrivateKey result = CredentialUtil.loadPrivateKey(encoded, algorithm);
            assertThat(result).isEqualTo(mockPrivateKey);
        }
    }

    @Test
    void testConvertPublicKeyToBase64() {
        var encodedKey = "mockedEncodedKey".getBytes();
        when(mockPublicKey.getEncoded()).thenReturn(encodedKey);

        try (var mockedCredentialUtil = mockStatic(CredentialUtil.class)) {
            mockedCredentialUtil
                    .when(() -> CredentialUtil.extractPublicKeyFromKeystore(any(KeyStore.class)))
                    .thenReturn(mockPublicKey);

            mockedCredentialUtil
                    .when(() -> CredentialUtil.convertPublicKeyToBase64(any(KeyStore.class)))
                    .thenCallRealMethod();

            var expectedBase64 = Base64.getEncoder().encodeToString(encodedKey);

            var result = CredentialUtil.convertPublicKeyToBase64(mockKeyStore);

            assertThat(result).isEqualTo(expectedBase64);
            verify(mockPublicKey).getEncoded();
        }
    }

    @Test
    void testLoadPrivateKey() throws Exception {
        var algorithm = "EC";
        var encodedKey = TestCertificateUtil.generateKeyPair().getPrivate().getEncoded();

        var result = CredentialUtil.loadPrivateKey(encodedKey, algorithm);

        assertThat(result).isNotNull();
        assertThat(result.getAlgorithm()).isEqualTo(algorithm);

        var keyFactory = KeyFactory.getInstance(algorithm);
        var keySpec = keyFactory.getKeySpec(result, PKCS8EncodedKeySpec.class);
        assertThat(keySpec.getEncoded()).isEqualTo(encodedKey);
    }

    @Test
    void extractCertificateFromKeystore_shouldThrowRuntimeWrapperException_whenKeyStoreExceptionOccurs()
            throws Exception {
        var mockException = new KeyStoreException("Test exception");
        when(mockKeyStore.aliases()).thenThrow(mockException);

        assertThrows(RuntimeWrapperException.class, () -> CredentialUtil.extractCertificateFromKeystore(mockKeyStore));
    }

    @Test
    void extractPrivateKeyFromKeystore_shouldThrowRuntimeWrapperException_whenExceptionOccurs() throws Exception {
        var keyEntryPassword = "testPassword";
        when(mockKeyStore.aliases()).thenReturn(Collections.enumeration(Collections.singletonList("testAlias")));
        when(mockKeyStore.getKey(anyString(), any(char[].class))).thenThrow(new KeyStoreException("Test exception"));

        assertThrows(
                RuntimeWrapperException.class,
                () -> CredentialUtil.extractPrivateKeyFromKeystore(mockKeyStore, keyEntryPassword));
    }
}
