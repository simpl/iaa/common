package eu.europa.ec.simpl.api.usersroles.v1.model;

public interface Credential {
    String getPublicKey();
}
