package eu.europa.ec.simpl.api.onboarding.v1.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;

public interface DocumentTemplate {
    UUID getId();

    @NotBlank
    String getDescription();

    @NotNull Boolean getMandatory();

    @NotNull MimeType getMimeType();

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    Instant getCreationTimestamp();

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    Instant getUpdateTimestamp();
}
