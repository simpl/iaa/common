package eu.europa.ec.simpl.api.securityattributesprovider.v1.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Set;
import java.util.UUID;

public interface IdentityAttribute {
    UUID getId();

    @NotBlank
    String getCode();

    @NotBlank
    String getName();

    String getDescription();

    @NotNull Boolean getAssignableToRoles();

    @NotNull Boolean getEnabled();

    Instant getCreationTimestamp();

    Instant getUpdateTimestamp();

    Set<String> getParticipantTypes();

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    Boolean getUsed();
}
