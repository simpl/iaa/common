description: Onboarding validation rule representation.
type: object
properties:
  id:
    type: string
    format: uuid
    description: Unique identifier of the rule
  ruleType:
    $ref: ./ValidationRuleType.yaml
  name:
    type: string
    description: A short readable name for the rule
    example: Document expiration date
  description:
    type: string
    description: A description of the rule.
    example: Checks the expiration date of the driving license
  validSince:
    type: string
    format: date-time
    description: The date since when the rule is valid
    example: "2024-12-16T17:31:27Z"
  validTo:
    type: string
    format: date-time
    description: The date until when the rule is valid.
    example: "2024-12-16T17:31:27Z"
  active:
    type: boolean
    description: Indicates if the rule is currently active
    example: true
  documentTemplate:
    $ref: ./DocumentTemplate.yaml
    description: Document template associated with the rule
  forApproval:
    type: boolean
    description: True if the rule concurs in the auto approval of the onboarding request
    example: true
  required:
    type: boolean
    description: True if the rule is required for the request to be passed to the next state
    example: true
  validationUri:
    type: string
    description: the http URI of the approval service, if the type is DOCUMENT_CONTENT_CHECK
    example: https://external-service.com/validate-document
  rules:
    type: array
    description: the list of the child rules, if the rule has type COMPOSITE
    items:
      $ref: ./OnboardingValidationRule.yaml
    example: []