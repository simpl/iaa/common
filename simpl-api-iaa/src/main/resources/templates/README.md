### OpenApi templates customization

Original templates can be found [here](https://github.com/OpenAPITools/openapi-generator/tree/master/modules/openapi-generator/src/main/resources/JavaSpring/libraries/spring-http-interface)

#### Using `ResponseEntity` for specific endpoints

The [OpenApi Spring generator](https://openapi-generator.tech/docs/generators/spring/) supports a global `useResponseEntity` configuration option
that allows all generated controller methods to return a `ResponseEntity`.

In order to return a response entity for certain endpoints only, a custom `x-response-entity` extension has been defined and the templates have been customized as follows:

1. `api.mustache`: remove `useResponseEntity` related conditions in order to always `import org.springframework.http.ResponseEntity`
2. `responseType.mustache`: replace `useResponseEntity` related conditions with `vendorExtensions.x-response-entity`
3. `returnTypes.mustache`: replace `useResponseEntity` related conditions with `vendorExtensions.x-response-entity`

##### Usage

In order to use the `ResponseEntity` return type for a given endpoint, just add the custom `x-response-entity` extension to the corresponding operation in the OpenApi specification:

```yaml
paths:
  /my/path:
    get:
      operationId: myOperation
      x-response-entity: true
```